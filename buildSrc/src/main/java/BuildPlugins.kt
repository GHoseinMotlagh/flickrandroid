object BuildPlugins {
    const val ANDROID_APPLICATION = "com.android.application"
    const val ANDROID_LIBRARY = "com.android.library"
    const val KOTLIN_ANDROID = "kotlin-android"
    const val KOTLIN_PARCELIZE = "kotlin-parcelize"
    const val KOTLIN_KAPT = "kotlin-kapt"
    const val HILT = "com.google.dagger.hilt.android"
    const val HILT_ANDROIDX = "dagger.hilt.android.plugin"
    const val KOTLIN_JETBRAINS = "org.jetbrains.kotlin.android"
    const val NAVIGATION = "androidx.navigation.safeargs.kotlin"
}