# FlickrAndroid



## what i did:

* Implement search functionality
* Implement a page for showing image in big size
* Implement quick search
* Add some unit tests and error handling.

### **Libraries/concepts used**

* Retrofit - for networking
* Hilt - for Dependency Injection pattern implementation
* Room - for local database
* Coil - for image loading
* Kotlin Coroutines & Kotlin Flow - for concurrency & reactive approach
* Jetpack Compose - for UI layer
* Gradle modularised project by features
* The Clean Architecture with MVVM pattern






